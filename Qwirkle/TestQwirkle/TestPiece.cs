﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace TestQwirkle
{
    [TestClass]
    public class TestPiece
    {
        [TestMethod]
        public void Testpieces()  
        {
            QColor red = QColor.red;        //couleurs
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;    //formes
            QShape clover = QShape.clover;
            Piece redcross = new Piece(red, cross);   //new piece
            Assert.AreEqual(cross, redcross.getform()); 
            Assert.AreEqual(red, redcross.getcolor()); 
            Assert.AreNotEqual(yellow, redcross.getcolor()); 
            Assert.AreNotEqual(clover, redcross.getform());
        }

        [TestMethod]
        public void TestCompability() //tests compatibilité 
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(true, redcross.Compatibility(redclover)); // pas même forme mais même couleur : compatible
            Assert.AreEqual(false, redcross.Compatibility(yellowclover)); // pas même form, pas même couleur : incompatible
        }
        [TestMethod]
        public void TestCompabilityForm() //tests compatibilité forme
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(false, redcross.CompatibilityForm(redclover)); //pas même forme : incompatible
            Assert.AreEqual(true, redclover.CompatibilityForm(yellowclover)); //même forme : compatible
        }
        [TestMethod]
        public void TestCompabilityColor() //tests compatibilité couleur
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(true, redcross.CompatibilityColor(redclover)); //même couleur : compatible
            Assert.AreEqual(false, redcross.CompatibilityColor(yellowclover)); //pas même couleur : incompatible
        }
    }
}
