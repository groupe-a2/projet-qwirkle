﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace TestQwirkle
{
    [TestClass]
    public class Testpioche
    {
        [TestMethod]
        public void Testaddpiece() //tests ajouter des pièces à la pioche
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowdiamond = new Piece(yellow, diamond);
            Pioche listp = new Pioche();
            listp.Addpieces(yellowdiamond);
            Assert.AreEqual(yellowdiamond, listp.getpieces()[0]);
            listp.Addpieces(redcross);
            Assert.AreEqual(2, listp.getpieces().Count);
            Assert.AreEqual(redcross, listp.getpieces()[1]);

        }
        [TestMethod]
        public void TestPioche()
        {
            Pioche listp = new Pioche();
            listp.Piocheinit();
            Assert.AreEqual(108, listp.getpieces().Count); //il y a bien 108 pièces au total dans le jeu
        }
    }
}
