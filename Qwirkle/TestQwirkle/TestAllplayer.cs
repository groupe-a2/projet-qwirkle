﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace TestQwirkle
{
    [TestClass]
    public class TestAllplayer
    {
        [TestMethod]
        public void testaddplayer() // tests ajouter des joueurs à la liste
        {
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            AllPlayer listp = new AllPlayer();
            Player Louis = new Player("Louis", 800, 2, hand); //nom, points, ordre de jeu, main
            Player Jean = new Player("Jean", 600, 3, hand1);
            listp.Addplayers(Louis);
            Assert.AreEqual(Louis, listp.getplayers()[0]);
            listp.Addplayers(Jean);
            Assert.AreEqual(2, listp.getplayers().Count); 
            Assert.AreEqual(Jean, listp.getplayers()[1]);
        }

        [TestMethod]
        public void testsetscore() //tests scores et les pseudos pour chaque joueur de la liste
        {
            List<Piece> hand = new List<Piece>();
            AllPlayer listp = new AllPlayer();
            Player Louis = new Player("Louis", 800, 2, hand);
            listp.Addplayers(Louis);
            string expected_introduction = "Louis:800"; //affichage nom, score


            Assert.AreEqual(expected_introduction, listp.setscore());
        }

        [TestMethod]
        public void testTurn() //tests ordre des joueurs 
        {
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            AllPlayer listp = new AllPlayer();
            Player Louis = new Player("Louis", 800, 2, hand1);
            Player Jean = new Player("Jean", 600, 3, hand);
            QColor red = QColor.red; QColor yellow = QColor.yellow; QColor blue = QColor.blue; //couleur
            QShape cross = QShape.cross; QShape diamond = QShape.diamond; QShape clover = QShape.clover; //forme
            Piece bluecross = new Piece(blue, cross); Piece redclover = new Piece(red, clover); Piece reddiamond = new Piece(red, diamond); //liste de pieces
            Piece yellowdiamond = new Piece(yellow, diamond); Piece bluediamond = new Piece(blue, diamond); Piece yellowcross = new Piece(yellow, cross);
            hand.Add(redclover); hand.Add(bluecross); hand.Add(yellowdiamond); hand.Add(reddiamond); hand.Add(bluediamond); hand.Add(yellowcross);
            hand1.Add(redclover); hand1.Add(bluediamond); hand1.Add(redclover); hand1.Add(yellowdiamond); hand1.Add(bluecross); hand1.Add(yellowcross);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            listp.Initturn();
            Assert.AreEqual(Louis, listp.getplayers()[0]);

        }

        [TestMethod]
        public void testwinner()
        {
            AllPlayer listp = new AllPlayer();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            List<Piece> hand2 = new List<Piece>();
            Player Louis = new Player("Louis", 800, 2, hand);
            Player Jean = new Player("Jean", 600, 3, hand);
            Player Henri = new Player("Henri", 450, 1, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            listp.Addplayers(Henri);
            Assert.AreEqual("Le gagnant est : Louis", listp.getWinner()); //affichage correct : points louis > points jean/henri
        }
    }
}
