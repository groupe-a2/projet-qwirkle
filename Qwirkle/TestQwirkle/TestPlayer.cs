﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace TestQwirkle
{
    [TestClass]
    public class Testplayer
    {
        [TestMethod]
        public void testplayer( //tests joueur
        {
            List<Piece> hand = new List<Piece>();
            Player Louis = new Player("Louis", 800, 3, hand);
            Assert.AreEqual("Louis", Louis.getname());
            Assert.AreEqual(800, Louis.getscore());
            Assert.AreEqual(3, Louis.getorder());

        }
        [TestMethod]
        public void Addpoints() //tests ajouter des points au score du joueur
        {
            List<Piece> hand = new List<Piece>();
            Player Louis = new Player("Louis", 800, 3, hand);
            Assert.AreEqual(800, Louis.getscore());
            Louis.addpoints(50); //+50 pts
            Assert.AreEqual(850, Louis.getscore());
        }

        [TestMethod]
        public void testinithand() //tests main des joueurs
        {
            AllPlayer listp = new AllPlayer();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            Pioche pioche = new Pioche();
            Player Louis = new Player("Louis", 800, 2, hand1);
            Player Jean = new Player("Jean", 600, 3, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            Player.inithand(pioche, listp); //init main joueur
            Assert.AreEqual(96, pioche.getpieces().Count); //108-6*2
            Assert.AreEqual(6, hand1.Count);
        }

        [TestMethod]
        public void testMAJhand() //tests main des joueurs à jour
        {
            AllPlayer listp = new AllPlayer();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            Pioche pioche = new Pioche();
            Player Louis = new Player("Louis", 800, 2, hand1);
            Player Jean = new Player("Jean", 600, 3, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            Player.inithand(pioche, listp);
            Jean.hand.Remove(hand[1]); 
            Assert.AreEqual(5, hand.Count); //6-1
            Player.MAJhand(pioche, listp); //met à jour la main
            Assert.AreEqual(6, hand.Count);
        }

    }
}
