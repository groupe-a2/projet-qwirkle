﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Pioche // classe de la pioche
    {
        public List<Piece> pieceslist;

        public Pioche() // la pioche est composée d'une liste de pièces
        {
            this.pieceslist = new List<Piece>();
        }
        public void Piocheinit() // initialise la pioche
        {

            for (int iteration = 1; iteration <= 3; iteration++)
            {
                foreach (QColor i in Enum.GetValues(typeof(QColor)))
                {
                    foreach (QShape j in Enum.GetValues(typeof(QShape)))
                    {
                        this.pieceslist.Add(new Piece(i, j));
                    }
                }

            }
        }
        public void Addpieces(Piece pieces) // permet d'ajouter des pièces à la pioche
        {
            this.pieceslist.Add(pieces);
        }

        public List<Piece> getpieces() // retourne la pioche
        {
            return this.pieceslist;
        }

    }
}
