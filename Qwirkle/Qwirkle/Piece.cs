﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public enum QColor // énumerations des couleurs des pièces
    {
        red, orange, yellow, green, blue, purple
    }
    public enum QShape // énumerations des formes des pièces
    {
        circle, square, diamond, star, clover, cross
    }
    public class Piece // classe pièce composée d'une couleur et d'une forme
    {
        public QColor Color
        {
            get;
            set;
        }
        public QShape Shape
        {
            get;
            set;
        }

        public Piece(QColor color, QShape shape) // initialise une pièce
        {
            this.Color = color;
            this.Shape = shape;
        }

        public QShape getform() // retourne la forme d'une pièce
        {
            return this.Shape;
        }
        public QColor getcolor() // retourne la couleur d'une pièce
        {
            return this.Color;
        }
        public bool Compatibility(Piece piece) // vérifie la compabilité entre 2 pièces
        {
            return (piece != null && (((this.Color == piece.Color) && (this.Shape != piece.Shape)) ||
                     ((this.Color != piece.Color) && (this.Shape == piece.Shape))));
        }
        public bool CompatibilityColor(Piece piece) // vérifie que 2 pièces ont la même couleur
        {
            return (piece != null && (((this.Color == piece.Color) && (this.Shape != piece.Shape))));
        }
        public bool CompatibilityForm(Piece piece) // vérifie que 2 pièces ont la même forme
        {
            return (piece != null && (((this.Color != piece.Color) && (this.Shape == piece.Shape))));
        }
    }
}
