﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Player         //Initialiser les joueurs 
    {
        public string playerID;
        public int playerPoints;
        public int playerturn;
        public List<Piece> hand;


        public Player(string playerID, int playerPoints, int playerturn, List<Piece> hand)
        {
            this.playerID = playerID;
            this.playerPoints = playerPoints;
            this.playerturn = playerturn;
            this.hand = hand;

        }
        public List<Piece> gethand() // retourne la main d'un joueur
        {
            return this.hand;
        }

        public static void inithand(Pioche pioche, AllPlayer playerslist) // initialise la main des joueurs
        {
            pioche.Piocheinit();

            foreach (var players in playerslist.getplayers())
            {
                for (int index1 = 1; index1 <= 6; index1++)
                {
                    var random = new Random();
                    int index = random.Next(pioche.getpieces().Count);
                    players.hand.Add(pioche.getpieces()[index]);
                    pioche.getpieces().Remove(pioche.getpieces()[index]);
                }
            }
        }
        public static void MAJhand(Pioche pioche, AllPlayer playerslist) // met la main des joueurs à jour
        {
            foreach (var players in playerslist.getplayers())
            {
                if (players.hand.Count < 6)
                {
                    while (players.hand.Count < 6)
                    {
                        var random = new Random();
                        int index = random.Next(pioche.getpieces().Count);
                        players.hand.Add(pioche.getpieces()[index]);
                        pioche.getpieces().Remove(pioche.getpieces()[index]);
                    }
                }
            }
        }

        public string getname() // retourne le pseudo d'un joueur
        {
            return this.playerID;
        }
        public int getscore() // retourne le score d'un joueur
        {
            return this.playerPoints;
        }
        public void addpoints(int points) // permet d'ajouter des points au score du joueur
        {
            playerPoints += points;
        }

        public int getorder() // retourne l'ordre de jeu du joueur
        {
            return this.playerturn;
        }

        public string Playernb(int nbjoueur) // permet à l'utilisateur d'entrer le nom des joueurs
        {
            int i;
            Console.WriteLine("saisissez le nombre de joueurs");
            for (i = 0; i <= 4; i++)
            {
                playerID = Console.ReadLine();
            }
            return playerID;
        }
    }
}