﻿
Imports QwirkleLib
Public Class frPagemenu

    Private Sub jouer_Click(sender As Object, e As EventArgs) Handles jouer.Click
        Me.Hide()
        nbjoueurs.Show()
    End Sub

    Private Sub scores_Click(sender As Object, e As EventArgs) Handles scores.Click
        Me.Hide()
        score.Show()
    End Sub

    Private Sub reglesdujeu_Click(sender As Object, e As EventArgs) Handles reglesdujeu.Click
        Me.Hide()
        regles.Show()
    End Sub

    Private Sub Quitter_Click(sender As Object, e As EventArgs) Handles quitter.Click
        Dim rep As DialogResult
        rep = MessageBox.Show("Voulez vous vraiment quitter?", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If rep = DialogResult.OK Then
            Me.Close()
        End If
    End Sub

    Private Sub FrPagemenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub
End Class
