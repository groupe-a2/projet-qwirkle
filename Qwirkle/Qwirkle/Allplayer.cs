﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class AllPlayer // code une liste de joueurs
    {
        List<Player> playerslist;
        public AllPlayer()
        {
            this.playerslist = new List<Player>();
        }
        public void Addplayers(Player player) // permet d'ajouter des joueurs à la liste
        {
            this.playerslist.Add(player);
        }
        public List<Player> getplayers() // retourne les joueurs présent dans la liste
        {
            return this.playerslist;
        }
        public void InitMax() // permet de compter le nombres de pièces que le joueurs peut placer à la suite
        {
            foreach (Player P in playerslist)
            {
                foreach (Piece i in P.hand)
                {
                    foreach (Piece j in P.hand)
                    {
                        int maxform = 0, maxcolor = 0;
                        if (i.CompatibilityColor(j))
                        {
                            maxcolor++;
                        }
                        if (i.CompatibilityForm(j))
                        {
                            maxform++;
                        }
                        if (maxform < maxcolor)
                        {
                            if (maxcolor > P.playerturn)
                            {
                                P.playerturn = maxcolor;
                            }

                        }
                        else
                        {
                            if (maxform > P.playerturn)
                            {
                                P.playerturn = maxform;
                            }

                        }
                    }
                }
            }
        }
        public void Initturn() // initialise l'ordre des joueurs à l'aide de la fonction InitMax
        {
            InitMax();
            foreach (Player P in playerslist)
            {
                int n = playerslist.Count() - 1;
                for (int i = n; i >= 1; i--)
                    for (int j = 2; j <= i; j++)
                        if (playerslist[j - 1].getorder() > playerslist[j].getorder())
                        {
                            Player temp = playerslist[j - 1];
                            playerslist[j - 1] = playerslist[j];
                            playerslist[j] = temp;
                        }
            }
        }

        public string setscore() // retourne les scores et les pseudos pour chaque joueur de la liste
        {
            int score;
            score = 0;
            string winner = null;
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Player P in playerslist)
            {
                score = P.playerPoints;
                winner = P.playerID;
                introduce_builder.AppendFormat("{0}:{1}\n", winner, score);
            }
            return introduce_builder.ToString().Trim();
        }
        public string getWinner() // Permet de déterminer le vainqueur
        {
            int MaxValue;
            MaxValue = 0;
            string winner = null;
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Player P in playerslist)
            {
                if (P.playerPoints > MaxValue)
                {
                    MaxValue = P.playerPoints;
                    winner = P.playerID;
                }
            }
            introduce_builder.AppendFormat("Le gagnant est : {0}", winner);
            return introduce_builder.ToString().Trim();
        }
    }
}
