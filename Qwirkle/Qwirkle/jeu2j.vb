﻿Public Class jeu2j

    'bug：
    '1.随机生成了棋子，但是一连生成3个一样的棋子，我找不到我写错的地方。
    '2.居中之后，旁边的一些地方不能放入棋子


    'bug
    '1. Les pièces ont été générées au hasard, mais trois pièces de la même pièce ont été générées successivement. Je n'ai pas pu trouver l'endroit où j'ai commis l'erreur.
    '2. Après le centrage, certains endroits adjacents ne peuvent pas être placés par les autres pions.
    '3.Dans la boucle ne peut pas être attribué (picl1)

    'nombre des manches
    Dim nbManche As Integer = 0

    'Bordure en damier
    Dim griW As Integer = 0
    Dim griH As Integer = 0

    '已填图案的最大边框
    'Le bord maximum de la pièce remplie
    Dim maxl As Integer = 6
    Dim maxt As Integer = 4
    Dim maxr As Integer = 6
    Dim maxb As Integer = 4

    'Taille du pic
    Dim w As Integer = 34

    'lbnbpions
    Dim nbpions As Integer = 96

    'pic.tag
    Dim t As Integer = 0



    '初始化
    'Initialiser
    Private Sub jeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen

        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132


        '初始化棋盘
        'Initialiser le damier

        Dim pic As PictureBox
        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < 13 And u < 9
            While i < 13
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y

                pic.Name = "pic" & i.ToString & "_" & u.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage


                Me.Controls.Add(pic)
                x += w

                If x = w * 13 + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While

        griH = 8
        griW = 12

        'Initialiser les pions
        grppionsplace(grpPions)
        Dim pions As PictureBox
        For i = 1 To 6
            pions = grpPions.Controls("pic" & i.ToString)
            majpions(pions)
        Next

        '第一轮只有中间能下
        'Au premier tour, seul le milieu peut jouer au pion.
        Dim Rien As Boolean = True
        For i = 0 To griW
            For u = 0 To griH
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                If pic.Image IsNot Nothing Then
                    Rien = False
                End If
            Next
        Next

        If Rien = True Then
            Dim picM As PictureBox
            picM = Controls("pic" & 6.ToString & "_" & 4.ToString)
            picM.BorderStyle = BorderStyle.Fixed3D
            picM.AllowDrop = True
            AddHandler picM.DragEnter, AddressOf picM_DragEnter
            AddHandler picM.DragDrop, AddressOf picM_DragDrop
        End If


        For i = 2 To 6
            AddHandler grpPions.Controls("pic" & i.ToString).MouseMove, AddressOf picPions_MouseMove
        Next

    End Sub
    '棋子
    'Mettre à jour le pions dans le grppions
    Private Sub majpions(ByVal pions As PictureBox)
        Dim x As Integer
        Dim y As Integer
        Dim xy As String


        Dim rnd As New Random

        Do
            x = rnd.Next(1, 7)
            y = rnd.Next(1, 7)
            xy = x.ToString & "." & y.ToString
            pions.Image = My.Resources.ResourceManager.GetObject(xy)
            pions.Image.Tag = xy
        Loop While pionsV(xy) = False

    End Sub
    'Vérifier que les pions générées aléatoirement sont valides
    Private Function pionsV(ByVal xy As String) As Boolean
        Dim n As Integer = 0
        Dim v As Boolean = True

        Dim xpyp As String

        Dim pic As PictureBox


        For i = maxl To maxr
            For u = maxt To maxb
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                If pic.Image IsNot Nothing Then
                    xpyp = pic.Image.Tag
                    If xy = xpyp Then
                        n += 1
                    End If
                End If
            Next
        Next
        For i = 1 To 6
            pic = grpPions.Controls("pic" & i.ToString)
            If pic.Image IsNot Nothing Then
                xpyp = pic.Image.Tag

                If xy = xpyp Then
                    n += 1
                End If
            End If
        Next


        If n > 3 Then
            v = False
        End If
        Return v
    End Function

    '棋盘位置
    'Position en grppions
    Private Sub grppionsplace(ByVal grp As GroupBox)
        Dim x As Integer = 84
        Dim y As Integer = 132 + griH * w + 50

        Dim lieu As New Point
        lieu.X = x
        lieu.Y = y

        grp.Location = lieu

    End Sub

    '拉拽棋子
    Private Sub picPions_MouseMove(sender As Object, e As MouseEventArgs) Handles pic1.MouseMove
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects

        If e.Button = MouseButtons.Left Then
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
                pic.AllowDrop = True
                AddHandler pic.DragEnter, AddressOf pic1_DragEnter
                AddHandler pic.DragDrop, AddressOf pic1_DragDrop
            End If
        End If

    End Sub

    '在棋盘放下棋子
    Private Sub picM_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub picM_DragDrop(sender As Object, e As DragEventArgs)
        Dim pic As PictureBox = sender
        Dim iu As String
        Dim tabiu As String()
        Dim i As Integer
        Dim u As Integer

        Dim clfr As String
        Dim tabclfr As String()
        Dim fr As Integer
        Dim cl As Integer

        Dim deplacerOriente As Char
        Dim x As Integer = 84
        Dim y As Integer = 132


        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w


        'vérification pose possible


        clfr = e.Data.GetData(DataFormats.Bitmap).Tag.Substring(0, 3)

        Dim picvoisin1 As PictureBox
        Dim picvoisin2 As PictureBox
        Dim picvoisin3 As PictureBox
        Dim picvoisin4 As PictureBox

        Dim picl1 As PictureBox

        Dim a As Int16

        Dim ligne As Boolean = True

        Dim colone As Boolean = True

        tabclfr = clfr.Split(CChar("."))
        cl = tabclfr(0)
        fr = tabclfr(1)

        iu = pic.Name.Substring(3)
        tabiu = iu.Split(CChar("_"))
        i = tabiu(0)
        u = tabiu(1)

        'pic = Controls("pic" & i.ToString & "_" & u.ToString) '被拖拽的棋子要放到的地方
        picvoisin1 = Controls("pic" & i.ToString & "_" & u - 1.ToString) '上
        picvoisin2 = Controls("pic" & i.ToString & "_" & u + 1.ToString) '下
        picvoisin3 = Controls("pic" & i - 1.ToString & "_" & u.ToString) '左
        picvoisin4 = Controls("pic" & i + 1.ToString & "_" & u.ToString) '右



        Dim correspendance1 As Boolean = True
        Dim correspendance2 As Boolean = True
        Dim correspendance3 As Boolean = True
        Dim correspendance4 As Boolean = True

        Dim top As Int16
        Dim un As Int16
        Dim le As Int16
        Dim ri As Int16

        If picvoisin1.Image IsNot Nothing Then
            top = 1
            correspendance1 = False
            If (picvoisin1.Image.Tag Like "*" & "." & fr.ToString AndAlso Not (picvoisin1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then '同形状
                a = 2

                If (picvoisin2.Image Is Nothing) OrElse (picvoisin2.Image.Tag Like "*." & fr.ToString AndAlso Not (picvoisin2.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                    Do
                        picl1 = Controls("pic" & i.ToString & "_" & u - a.ToString) 'Dans la boucle ne peut pas être attribué
                        a += 1

                        If picl1.Image IsNot Nothing OrElse (picl1.Image.Tag Like "*." & fr.ToString AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                            correspendance1 = True
                            top += 1

                        End If

                    Loop While （correspendance1 = True） AndAlso （Controls("pic" & i.ToString & "_" & u - a.ToString) IsNot Nothing） AndAlso （a <= 6）

                End If
            End If
            If (picvoisin1.Image.Tag Like cl.ToString & ".*" AndAlso Not (picvoisin1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then '同颜色
                a = 2

                If (picvoisin2.Image Is Nothing) Or (picvoisin2.Image.Tag Like cl.ToString & ".*" AndAlso Not (picvoisin2.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                    Do
                        picl1 = Controls("pic" & i.ToString & "_" & u - a.ToString)
                        a += 1
                        If picl1.Image.Tag Like cl.ToString & ".*" AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag) Then
                            correspendance1 = True
                            top += 1

                        End If

                    Loop While （correspendance1 = True） AndAlso （Controls("pic" & i.ToString & "_" & u - a.ToString) IsNot Nothing） AndAlso （a <= 6）
                End If
            End If

        End If

        If picvoisin3.Image IsNot Nothing Then
            le = 1
            correspendance2 = False
            If (picvoisin3.Image.Tag Like "*." & fr.ToString AndAlso Not (picvoisin3.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then '同形状
                a = 2

                If （picvoisin4.Image Is Nothing） OrElse (picvoisin4.Image.Tag Like "*." & fr.ToString() AndAlso String.Compare(picvoisin4.Image.Tag, clfr) = 0) Then
                    Do
                        picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                        a += 1
                        If (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like "*." & fr.ToString AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                            correspendance2 = True
                            le += 1

                        End If

                    Loop While （correspendance2 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）

                End If
            End If
            If (picvoisin3.Image.Tag Like cl.ToString & ".*" AndAlso Not (picvoisin3.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then '同颜色
                a = 2
                If （picvoisin4.Image Is Nothing） OrElse (picvoisin4.Image.Tag Like cl.ToString & ".*" AndAlso String.Compare(picvoisin4.Image.Tag, clfr) = 0) Then
                    Do
                        picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                        a += 1
                        If (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like cl.ToString & ".*" AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                            correspendance2 = True
                            le += 1

                        End If

                    Loop While （correspendance2 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）
                End If
            End If

        End If


        If picvoisin2.Image IsNot Nothing Then
            correspendance3 = False
            un = 1
            If （(picvoisin2.Image.Tag Like "*" & "." & fr.ToString） AndAlso （String.Compare(picvoisin2.Image.Tag, clfr) <> 0)） Then '同形状
                a = 2
                Do
                    picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                    a += 1
                    If (picl1.Image Is Nothing) OrElse (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like "*" & "." & fr.ToString AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                        correspendance3 = True
                        un += 1
                    End If

                Loop While （correspendance3 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）

            End If
            If （(picvoisin2.Image.Tag Like cl.ToString & ".*"） AndAlso （String.Compare(picvoisin2.Image.Tag, clfr) <> 0)） Then '同颜色
                a = 2
                Do
                    picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                    a += 1
                    If (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like cl.ToString & ".*" AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                        correspendance3 = True
                        un += 1
                    End If

                Loop While （correspendance3 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）

            End If

        End If

        If picvoisin4.Image IsNot Nothing Then
            ri = 1
            correspendance4 = False
            If （(picvoisin4.Image.Tag Like "*" & "." & fr.ToString） AndAlso (（picvoisin4.Image.Tag <> e.Data.GetData(DataFormats.Bitmap).Tag))） Then '同形状
                a = 2
                Do
                    picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                    a += 1
                    If (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like "*" & "." & fr.ToString AndAlso (picl1.Image.Tag <> e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                        correspendance4 = True
                        ri += 1
                    Else
                        correspendance4 = False
                    End If

                Loop While （correspendance4 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）

            End If
            If （(picvoisin4.Image.Tag Like cl.ToString & ".*"） AndAlso （(picvoisin4.Image.Tag <> e.Data.GetData(DataFormats.Bitmap).Tag)）) Then '同颜色
                a = 2
                Do
                    picl1 = Controls("pic" & i - a.ToString & "_" & u.ToString)
                    a += 1
                    If (picl1.Image Is Nothing) OrElse (picl1.Image.Tag Like cl.ToString & ".*" AndAlso Not (picl1.Image.Tag = e.Data.GetData(DataFormats.Bitmap).Tag)) Then
                        correspendance4 = True
                        ri += 1

                    End If

                Loop While （correspendance4 = True） AndAlso （Controls("pic" & i - a.ToString & "_" & u.ToString) IsNot Nothing） AndAlso （a <= 6）

            End If


        End If

        Dim picl2 As PictureBox
        'colone = True
        'ligne = True
        Dim j As Int16

        If top > 0 AndAlso un > 0 Then
            For j = 1 To i = top
                For a = 1 To a = un
                    picl1 = Controls("pic" & i.ToString & "_" & u - j.ToString)
                    picl2 = Controls("pic" & i.ToString & "_" & u + a.ToString)
                    If picl1.Image.Tag = picl2.Image.Tag Then
                        colone = False
                    End If
                Next
            Next

        End If
        If le > 0 AndAlso ri > 0 Then
            For j = 1 To i = le
                For a = 1 To a = ri
                    picl1 = Controls("pic" & i - j.ToString & "_" & u.ToString)
                    picl2 = Controls("pic" & i + a.ToString & "_" & u + a.ToString)
                    If picl1.Image.Tag = picl2.Image.Tag Then
                        ligne = False

                    End If
                Next
            Next

        End If



        'If correspendance1 = True AndAlso correspendance2 = True AndAlso correspendance3 = True AndAlso correspendance4 = True AndAlso colone = True AndAlso ligne = True Then
        '    pic.AllowDrop = True
        'End If

        If correspendance1 = True AndAlso correspendance2 = True AndAlso correspendance3 = True AndAlso correspendance4 = True AndAlso colone = True AndAlso ligne = True Then
            pic.AllowDrop = True
            pic.Image = e.Data.GetData(DataFormats.Bitmap)
            t += 1
            pic.Tag = t.ToString


            AddHandler pic.MouseMove, AddressOf grpPions_MouseMove
            ouvrir(pic)

            'max
            If i > maxr Then
                maxr = i
            End If

            If i < maxl Then
                maxl = i
            End If

            If u < maxt Then
                maxt = u
            End If

            If u > maxb Then
                maxb = u
            End If
        Else
            e.Effect = DragDropEffects.None
        End If

        'pic.Image = e.Data.GetData(DataFormats.Bitmap)
        'pic.AllowDrop = False
        'AddHandler pic.MouseMove, AddressOf grpPions_MouseMove
        'ouvrir(pic) 




        'Centrer les pions
        '左移 Déplacer à gauche
        If (griW - maxr - maxl) = -2 Then
            deplacerOriente = "L"
            deplacement(deplacerOriente)
        End If

        '右移 à droite
        If (griW - maxr - maxl) = 2 Then
            deplacerOriente = "R"
            deplacement(deplacerOriente)
        End If

        '上移 Monter
        If (griH - maxb - maxt) = -2 Then
            deplacerOriente = "T"
            deplacement(deplacerOriente)
        End If

        '下移 Descendre
        If (griH - maxb - maxt) = 2 Then
            deplacerOriente = "B"
            deplacement(deplacerOriente)
        End If


        '下加一行 Ajouter une ligne en bas
        If (griH - maxb) = 1 Then
            Dim n As Integer

            For n = 0 To griW
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y + (griH + 1) * w

                pic.Name = "pic" & n.ToString & "_" & griH + 1.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage

                Me.Controls.Add(pic)
                x += w
            Next
            griH += 1
            grppionsplace(grpPions)
        End If

        '左加一行 Ajouter une ligne à gauche
        If (griW - maxr) = 1 Then
            Dim n As Integer

            For n = 0 To griH
                pic = New PictureBox
                Dim lieu As New Point
                lieu.X = x + (griW + 1) * w
                lieu.Y = y

                pic.Name = "pic" & n.ToString & "_" & griW + 1.ToString
                pic.Size = taille
                pic.Location = lieu
                pic.Visible = True
                pic.BackColor = Color.Gray
                pic.BorderStyle = BorderStyle.FixedSingle
                pic.SizeMode = PictureBoxSizeMode.StretchImage

                Me.Controls.Add(pic)
                y += w
            Next
            griW += 1
        End If
    End Sub

    'returner）
    Private Sub grpPions_MouseMove(sender As Object, e As MouseEventArgs)
        Dim rep As DragDropEffects
        Dim pic As PictureBox = sender
        sender.allowdrop = False
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                pic.Image = Nothing
                RemoveHandler pic.MouseMove, AddressOf grpPions_MouseMove
                pic.AllowDrop = True
                fermer(pic)
                AddHandler pic.DragEnter, AddressOf picM_DragEnter
                AddHandler pic.DragDrop, AddressOf picM_DragDrop
            End If
        End If

    End Sub
    Private Sub pic1_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub pic1_DragDrop(sender As Object, e As DragEventArgs)
        Dim pica As PictureBox = sender
        'vérification pose possible

        pica.Image = e.Data.GetData(DataFormats.Bitmap)
        pica.AllowDrop = False
        RemoveHandler pica.DragEnter, AddressOf pic1_DragEnter
        RemoveHandler pica.DragDrop, AddressOf pic1_DragDrop

    End Sub
    '关闭周围 Supprimer le pions et fermer les circonférences
    Private Sub fermer(ByVal picF As PictureBox)
        Dim iu As String
        Dim tabiu As String()
        Dim i As Integer
        Dim u As Integer
        Dim picVoisins As List(Of PictureBox)

        picF.Image = Nothing
        picF.AllowDrop = True
        picF.BorderStyle = BorderStyle.Fixed3D

        iu = picF.Name.Substring(3)
        tabiu = iu.Split(CChar("_"))
        i = tabiu(0)
        u = tabiu(1)

        picVoisins = New List(Of PictureBox)
        picVoisins.Add(Controls("pic" & i + 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i - 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u + 1.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u - 1.ToString))


        For Each V As PictureBox In picVoisins
            If V.Image Is Nothing Then
                V.AllowDrop = False
                V.BorderStyle = BorderStyle.FixedSingle
            End If
        Next
    End Sub

    '开放周围  ouvrir les circonférences
    Private Sub ouvrir(ByVal picV As PictureBox)
        Dim iu As String
        Dim tabiu As String()
        Dim i As Integer
        Dim u As Integer
        Dim picVoisins As List(Of PictureBox)

        iu = picV.Name.Substring(3)
        tabiu = iu.Split(CChar("_"))
        i = tabiu(0)
        u = tabiu(1)

        picVoisins = New List(Of PictureBox)
        picVoisins.Add(Controls("pic" & i + 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i - 1.ToString & "_" & u.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u + 1.ToString))
        picVoisins.Add(Controls("pic" & i.ToString & "_" & u - 1.ToString))


        For Each V As PictureBox In picVoisins
            If V.Image Is Nothing Then
                V.AllowDrop = True
                V.BorderStyle = BorderStyle.Fixed3D
                AddHandler V.DragEnter, AddressOf picM_DragEnter
                AddHandler V.DragDrop, AddressOf picM_DragDrop
            End If
        Next
    End Sub

    '移动
    'Centrer les pions
    Private Sub deplacement(ByVal oriente As Char)
        Dim picD As PictureBox
        Dim pic As PictureBox
        Dim i As Integer
        Dim u As Integer
        Dim img As String

        Select Case oriente
            Case "L"
                For i = maxl To maxr
                    For u = maxt To maxb
                        pic = Controls("pic" & i.ToString & "_" & u.ToString)
                        picD = Controls("pic" & i - 1.ToString & "_" & u.ToString)
                        If pic.Image IsNot Nothing Then
                            img = pic.Image.Tag.ToString()
                            picD.Image = My.Resources.ResourceManager.GetObject(img)
                            picD.AllowDrop = False
                            fermer(pic)
                            ouvrir(picD)
                        End If
                    Next
                Next
                maxl -= 1
                maxr -= 1
            Case "T"

                For u = maxt To maxb
                    For i = maxl To maxr
                        pic = Controls("pic" & i.ToString & "_" & u.ToString)
                        picD = Controls("pic" & i.ToString & "_" & u - 1.ToString)
                        If pic.Image IsNot Nothing Then
                            picD.Image = pic.Image
                            picD.AllowDrop = False
                            fermer(pic)
                            ouvrir(picD)
                        End If
                    Next
                Next
                maxt = 1
                maxb -= 1
            Case "R"
                For i = maxr To maxl Step -1
                    For u = maxt To maxb
                        pic = Controls("pic" & i.ToString & "_" & u.ToString)
                        picD = Controls("pic" & i + 1.ToString & "_" & u.ToString)
                        If pic.Image IsNot Nothing Then
                            picD.Image = pic.Image
                            picD.AllowDrop = False
                            fermer(pic)
                            ouvrir(picD)
                        End If
                    Next
                Next
                maxl += 1
                maxr += 1
            Case "B"

                For u = maxb To maxt Step -1
                    For i = maxl To maxr
                        pic = Controls("pic" & i.ToString & "_" & u.ToString)
                        picD = Controls("pic" & i.ToString & "_" & u + 1.ToString)
                        If pic.Image IsNot Nothing Then
                            picD.Image = pic.Image
                            picD.AllowDrop = False
                            fermer(pic)
                            ouvrir(picD)
                        End If
                    Next
                Next
                maxt += 1
                maxb += 1
        End Select

    End Sub

    '摁下勾键 轮数加一 重新分配棋子 le bouton "crochet"
    Private Sub cmdManche_Click(sender As Object, e As EventArgs) Handles cmdManche.Click
        nbManche += 1


        Dim picV As PictureBox
        For i = maxl To maxr
            For u = maxt To maxb
                picV = Controls("pic" & i.ToString & "_" & u.ToString)
                If picV.Image IsNot Nothing Then
                    picV.Enabled = False
                    nbpions -= 1
                End If
            Next
        Next

        'lbnbpions changer
        lbnbpions.Text = nbpions

        Dim pions As PictureBox
        For i = 1 To 6
            pions = grpPions.Controls("pic" & i.ToString)
            If pions.Image Is Nothing Then
                majpions(pions)
                AddHandler pions.MouseMove, AddressOf picPions_MouseMove
            End If
        Next


    End Sub


    '变大 Devenir plus grand
    Private Sub CmdG_Click(sender As Object, e As EventArgs) Handles cmdG.Click
        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132

        w += 1

        Dim pic As PictureBox

        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < (griW + 1) And u < (griH + 1)
            While i < (griW + 1)
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                pic.Size = taille
                pic.Location = lieu

                Me.Controls.Add(pic)
                x += w

                If x = w * (griW + 1) + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While
    End Sub

    '变小 Devenir plus petit
    Private Sub CmdP_Click(sender As Object, e As EventArgs) Handles cmdP.Click
        Dim i As Byte = 0
        Dim u As Byte = 0
        Dim x As Integer = 84
        Dim y As Integer = 132

        w -= 1

        Dim pic As PictureBox

        Dim taille As New System.Drawing.Size
        taille.Height = w
        taille.Width = w

        While i < (griW + 1) And u < (griH + 1)
            While i < (griW + 1)
                Dim lieu As New Point
                lieu.X = x
                lieu.Y = y
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                pic.Size = taille
                pic.Location = lieu

                Me.Controls.Add(pic)
                x += w

                If x = w * (griW + 1) + 84 Then
                    x = 84
                    y += w
                End If
                i += 1
            End While
            u += 1
            i = 0
        End While
    End Sub
    'Clé de retour
    Private Sub CmdReturner_Click(sender As Object, e As EventArgs) Handles cmdReturner.Click

        Dim pic As PictureBox
        Dim pion As PictureBox
        Dim i As Integer
        Dim j As Integer = 1
        For i = maxl To maxr
            For u = maxt To maxb
                pic = Controls("pic" & i.ToString & "_" & u.ToString)
                If pic.Image IsNot Nothing Then
                    If pic.Tag = t.ToString Then
                        Do
                            pion = grpPions.Controls("pic" & j.ToString)
                            If pion.Image Is Nothing Then
                                pion.Image = pic.Image
                                pic.Image = Nothing
                                fermer(pic)
                                pic.AllowDrop = True
                            Else j += 1
                            End If
                        Loop While j < 7
                        t -= 1
                    End If
                End If
            Next
        Next
    End Sub

    'Bouton SST
    Private Sub CmdSST_Click(sender As Object, e As EventArgs) Handles cmdSST.Click
        nbManche += 1
    End Sub
    'Quitter
    Private Sub Quitter_Click(sender As Object, e As EventArgs) Handles quitter.Click
        Dim rep As DialogResult
        rep = MessageBox.Show("Voulez vous vraiment quitter?", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If rep = DialogResult.OK Then
            Me.Close()
        End If
    End Sub

    Private Sub cmdMenu_Click(sender As Object, e As EventArgs) Handles cmdMenu.Click
        Dim rep As DialogResult
        rep = MessageBox.Show("En réalisant cette opération, vous abandonnez la partie. Voulez-vous vraimentle faire?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If rep = DialogResult.Yes Then
            Me.Hide()
            frPagemenu.Show()
        End If
    End Sub
    'Click
    Private Sub CmdAbandonner_Click(sender As Object, e As EventArgs) Handles cmdAbandonner.Click
        Dim rep As DialogResult
        rep = MessageBox.Show("Voulez vous vraiment abandonner la partie?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If rep = DialogResult.Yes Then
            Me.Hide()
            frPagemenu.Show()
        End If
    End Sub

    Private Sub j1_TextChanged(sender As Object, e As EventArgs) Handles j1.TextChanged
        j1.Text = formulaire2j.infoj1.Text + " :"
    End Sub

    Private Sub j2_TextChanged(sender As Object, e As EventArgs) Handles j2.TextChanged
        j2.Text = formulaire2j.infoj2.Text + " :"
    End Sub

    'DraDrop
    Private Sub jeu2j_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim echanger As Button

        echanger = Me.Controls("cmdEchanger")
        echanger = cmdEchanger.Controls("cmdEchanger")
        cmdEchanger.AllowDrop = True
        For i = 1 To 6
            AddHandler grpPions.Controls("pic" & i.ToString).DragEnter, AddressOf cmdEchanger_DragDrop
        Next
    End Sub

    Private Sub pic_MouseMove(sender As Object, e As MouseEventArgs) Handles pic1.MouseMove
        Dim pic As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            rep = 0
            rep = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
        End If
    End Sub

    Private Sub cmdEchanger_DragEnter(sender As Object, e As DragEventArgs) Handles cmdEchanger.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub cmdEchanger_DragDrop(sender As Object, e As DragEventArgs) Handles cmdEchanger.DragDrop
        Dim pic As Button = sender
        pic.Image = Nothing
    End Sub
End Class
